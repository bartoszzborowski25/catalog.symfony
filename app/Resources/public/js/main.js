function menufixed (boolen_menu) {
    if (boolen_menu)
        $("#nav").addClass("menu-fixed");
    else 
        $("#nav").removeClass("menu-fixed");
        
}

$(window).scroll(function(){
    var height_body = $('body').scrollTop();
    if (height_body >= 40)
        menufixed(true);
    else 
        menufixed(false);
})
$(document).ready(function(){

/* ==========================================================================
   Płynne Przewijanie
   ========================================================================== */

	$('a[href^="#"]').on('click', function(event) {

		var target = $( $(this).attr('href') );

		if( target.length ) {
			event.preventDefault();
			$('html, body').animate({
				scrollTop: target.offset().top
			}, 1500);
		}
	});

});
