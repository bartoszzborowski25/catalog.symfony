<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appProdUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = array(
        '_welcome' => array (  0 =>   array (    0 => '_locale',  ),  1 =>   array (    '_locale' => 'pl',    '_controller' => 'AppBundle\\Controller\\StartController::startAction',  ),  2 =>   array (    '_locale' => 'pl|en',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => 'pl|en',      3 => '_locale',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        '_producers' => array (  0 =>   array (    0 => '_locale',  ),  1 =>   array (    '_locale' => 'pl',    '_controller' => 'AppBundle\\Controller\\StartController::producersAction',  ),  2 =>   array (    '_locale' => 'pl|en',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/producers',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => 'pl|en',      3 => '_locale',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        '_test' => array (  0 =>   array (    0 => '_locale',  ),  1 =>   array (    '_locale' => 'pl',    '_controller' => 'AppBundle\\Controller\\StartController::newAction',  ),  2 =>   array (    '_locale' => 'pl|en',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/test',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => 'pl|en',      3 => '_locale',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    );
        }
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
