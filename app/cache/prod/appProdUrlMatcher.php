<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // _welcome
        if (preg_match('#^/(?P<_locale>pl|en)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_welcome')), array (  '_locale' => 'pl',  '_controller' => 'AppBundle\\Controller\\StartController::startAction',));
        }

        // _producers
        if (preg_match('#^/(?P<_locale>pl|en)/producers$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_producers')), array (  '_locale' => 'pl',  '_controller' => 'AppBundle\\Controller\\StartController::producersAction',));
        }

        // _test
        if (preg_match('#^/(?P<_locale>pl|en)/test$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_test')), array (  '_locale' => 'pl',  '_controller' => 'AppBundle\\Controller\\StartController::newAction',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
