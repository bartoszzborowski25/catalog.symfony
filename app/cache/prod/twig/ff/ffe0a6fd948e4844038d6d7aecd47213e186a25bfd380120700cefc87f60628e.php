<?php

/* base.html.twig */
class __TwigTemplate_0a64e033bbcf622eafb367eec441a6cdb461bcdf00c6e6b64f36cced5e38573e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'nav_block' => array($this, 'block_nav_block'),
            'body' => array($this, 'block_body'),
            'footer_info' => array($this, 'block_footer_info'),
            'javascripts_user' => array($this, 'block_javascripts_user'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\" lang=\"\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\" lang=\"\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js lt-ie9\" lang=\"\"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class=\"no-js\" lang=\"\">
    <!--<![endif]-->
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"description\" content=\"\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/fonts/stylesheet.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/bootstrap-theme.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/stylesheets/main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <!--    =================== owl carousel =====================-->
        <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/owl.carousel.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <!--    =================== Animate=====================-->
        <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/animate.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        
        <!--    =================== Font Awsome =====================-->
        <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css\" rel=\"stylesheet\" integrity=\"sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1\" crossorigin=\"anonymous\">
        <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"), "html", null, true);
        echo "\"/></script>
    </head>
    <body>
        ";
        // line 28
        $this->displayBlock('nav_block', $context, $blocks);
        // line 62
        echo "        ";
        $this->displayBlock('body', $context, $blocks);
        // line 63
        echo "        <!--   Contact section      -->
        <section id=\"contact\">
            <div class=\"container catalog\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h2 class=\"text-center\">
                        Formularz kontaktowy
                        <div class=\"space-100\"></div>
                        <div class=\"row\">
                            <div class=\"col-sm-8\">
                                <div class=\"row\">
                                    <form id=\"form-javascript\" action=\"";
        // line 74
        echo $this->env->getExtension('routing')->getPath("_test");
        echo "\" method=\"post\">
                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <input placeholder=\"Imię i nazwisko\" required name=\"name\" type=\"text\" class=\"form-control\" id=\"usr\">
                                                <p class=\"alert-form\"></p>
                                            </div>
                                        </div>
                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <input placeholder=\"Nazwa firmy\" required name=\"name_company\" type=\"text\" class=\"form-control\" id=\"pwd\">
                                                <p class=\"alert-form\"></p>
                                            </div>
                                        </div>
                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <input placeholder=\"Numer telefonu\" required name=\"tel_number\" type=\"tel\" class=\"form-control\" id=\"usr\">
                                                <p class=\"alert-form\"></p>
                                            </div>
                                        </div>
                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <input placeholder=\"Adres email\" required name=\"email_adres\" type=\"email\" class=\"form-control\" id=\"pwd\">
                                                <p class=\"alert-form\"></p>
                                            </div>
                                        </div>
                                        <div class=\"col-sm-12\">
                                            <div class=\"form-group\">
                                                <textarea placeholder=\"Treść wiadomości\" name=\"content_messege\" class=\"form-control\" rows=\"9\" id=\"comment\"></textarea>
                                                <p class=\"alert-form\"></p>
                                            </div>
                                        </div>
                                        <div class=\"col-sm-3\">
                                            <div class=\"space-20\"></div>
                                            <button type=\"submit\" id=\"form-button-main\" class=\"btn pull-left btn-default-catalogy\">Wyślij</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class=\"col-sm-4\">
                                <div class=\"contact-right\">
                                    <div class=\"contact-right-logo\">
                                        <img src=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Vector-Smart-Object-email.png"), "html", null, true);
        echo "\" alt=\"adres\" class=\"img-responsive\">
                                    </div>
                                    <div class=\"contact-right-title\">
                                        <h3>dane <br> teleadresowe</h3>
                                        <p>kontakt z właścicielem serwisu</p>
                                    </div>
                                    <div class=\"contact-right-info\">
                                        <p><a href=\"tel: +48 600 215 364\">+48 600 215 364</a></p>
                                        <p><a href=\"mailto: contact@catalog.com\">contact@catalog.com</a></p>
                                        <div class=\"space-10\"></div>
                                        <p>Wolsztyńska 18</p>
                                        <p>65-415 Wrocław</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </h2>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <div class=\"container catalog\">
                <div class=\"row\">
                    <div class=\"col-sm-4\">";
        // line 139
        $this->displayBlock('footer_info', $context, $blocks);
        echo "</div>
                    <div class=\"col-sm-8 text-right\"><a href=\"#nav-section-up\">Up</a></div>
                </div>
            </div>
        </footer>
        <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"/></script>
        <script src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/vendor/bootstrap.min.js"), "html", null, true);
        echo "\"/></script>
        <script type=\"text/javascript\" src=\"http://maps.google.com/maps/api/js?sensor=true\"></script>
        <script>
            var map;
            var myCenter = new google.maps.LatLng(53, -1.33);
            var marker = new google.maps.Marker({
                position: myCenter
            });

            function initialize() {
                var mapProp = {
                    center: myCenter,
                    zoom: 14,
                    draggable: false,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                map = new google.maps.Map(document.getElementById(\"map-canvas\"), mapProp);
                marker.setMap(map);

                google.maps.event.addListener(marker, 'click', function () {

                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);

                });
            };
            google.maps.event.addDomListener(window, 'load', initialize);

            google.maps.event.addDomListener(window, \"resize\", resizingMap());

            \$('#myMapModal').on('show.bs.modal', function () {
                //Must wait until the render of the modal appear, thats why we use the resizeMap and NOT resizingMap!! ;-)
                resizeMap();
            })

            function resizeMap() {
                if (typeof map == \"undefined\") return;
                setTimeout(function () {
                    resizingMap();
                }, 400);
            }

            function resizingMap() {
                if (typeof map == \"undefined\") return;
                var center = map.getCenter();
                google.maps.event.trigger(map, \"resize\");
                map.setCenter(center);
            }
        </script>
        <script src=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/main.js"), "html", null, true);
        echo "\"/></scirpt>
        ";
        // line 197
        $this->displayBlock('javascripts_user', $context, $blocks);
        // line 200
        echo "    </body>
</html>";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        echo "Catalog.com";
    }

    // line 28
    public function block_nav_block($context, array $blocks = array())
    {
        // line 29
        echo "            <section id=\"nav\">
                <div id=\"nav-section-up\" class=\"container catalog\">
                    <div class=\"row\">
                        <nav class=\"navbar navbar-default navbar-default-start\">
                            <div class=\"container-fluid\">
                                <!-- Brand and toggle get grouped for better mobile display  -->
                                <div class=\"navbar-header\">
                                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
                                    <span class=\"sr-only\">Toggle navigation</span>
                                    <span class=\"icon-bar\"></span>
                                    <span class=\"icon-bar\"></span>
                                    <span class=\"icon-bar\"></span>
                                    </button>
                                    <a class=\"navbar-brand\" href=\"";
        // line 42
        echo $this->env->getExtension('routing')->getPath("_welcome");
        echo "\"><img class=\"img-responsive\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Vector-Smart-Object-copy-4.png"), "html", null, true);
        echo "\" alt=\"logo\"/></a>
                                </div>
                                <!-- Collect the nav links, forms, and other content for toggling  -->
                                <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                                    <ul class=\"nav navbar-nav navbar-right\">
                                        <li><a href=\"";
        // line 47
        echo $this->env->getExtension('routing')->getPath("_welcome");
        echo "\">Home</a></li>
                                        <li><a href=\"#\">";
        // line 48
        echo $this->env->getExtension('translator')->getTranslator()->trans("Po pracy", array(), "messages");
        echo "</a></li>
                                        <li><a href=\"#\">Kontakt</a></li>
                                        <li><a href=\"#\">Regulamin</a></li>
                                        <li><a href=\"#\">O nas</a></li>
                                    </ul>
                                </div>
                                <!-- /.navbar-collapse  -->
                            </div>
                            <!-- /.container-fluid -->
                        </nav>
                    </div>
                </div>
            </section>
        ";
    }

    // line 62
    public function block_body($context, array $blocks = array())
    {
    }

    // line 139
    public function block_footer_info($context, array $blocks = array())
    {
        echo "All rights reserved by catalog.com";
    }

    // line 197
    public function block_javascripts_user($context, array $blocks = array())
    {
        // line 198
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  310 => 198,  307 => 197,  301 => 139,  296 => 62,  278 => 48,  274 => 47,  264 => 42,  249 => 29,  246 => 28,  240 => 11,  235 => 200,  233 => 197,  229 => 196,  175 => 145,  166 => 139,  139 => 115,  95 => 74,  82 => 63,  79 => 62,  77 => 28,  71 => 25,  64 => 21,  59 => 19,  54 => 17,  50 => 16,  46 => 15,  42 => 14,  36 => 11,  24 => 1,);
    }
}
/* <!doctype html>*/
/* <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->*/
/* <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->*/
/* <!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->*/
/* <!--[if gt IE 8]><!-->*/
/* <html class="no-js" lang="">*/
/*     <!--<![endif]-->*/
/*     <head>*/
/*         <meta charset="utf-8">*/
/*         <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">*/
/*         <title>{% block title %}Catalog.com{% endblock %}</title>*/
/*         <meta name="description" content="">*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*         <link href="{{ asset('bundles/framework/fonts/stylesheet.css') }}" rel="stylesheet" />*/
/*         <link href="{{ asset('bundles/framework/css/bootstrap.min.css') }}" rel="stylesheet" />*/
/*         <link href="{{ asset('bundles/framework/css/bootstrap-theme.min.css') }}" rel="stylesheet" />*/
/*         <link href="{{ asset('bundles/framework/stylesheets/main.css') }}" rel="stylesheet" />*/
/*         <!--    =================== owl carousel =====================-->*/
/*         <link href="{{ asset('bundles/framework/css/owl.carousel.css') }}" rel="stylesheet" />*/
/*         <!--    =================== Animate=====================-->*/
/*         <link href="{{ asset('bundles/framework/css/animate.css') }}" rel="stylesheet" />*/
/*         */
/*         <!--    =================== Font Awsome =====================-->*/
/*         <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">*/
/*         <script src="{{ asset('bundles/framework/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"/></script>*/
/*     </head>*/
/*     <body>*/
/*         {% block nav_block %}*/
/*             <section id="nav">*/
/*                 <div id="nav-section-up" class="container catalog">*/
/*                     <div class="row">*/
/*                         <nav class="navbar navbar-default navbar-default-start">*/
/*                             <div class="container-fluid">*/
/*                                 <!-- Brand and toggle get grouped for better mobile display  -->*/
/*                                 <div class="navbar-header">*/
/*                                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">*/
/*                                     <span class="sr-only">Toggle navigation</span>*/
/*                                     <span class="icon-bar"></span>*/
/*                                     <span class="icon-bar"></span>*/
/*                                     <span class="icon-bar"></span>*/
/*                                     </button>*/
/*                                     <a class="navbar-brand" href="{{ path('_welcome') }}"><img class="img-responsive" src="{{ asset('bundles/framework/img/Vector-Smart-Object-copy-4.png') }}" alt="logo"/></a>*/
/*                                 </div>*/
/*                                 <!-- Collect the nav links, forms, and other content for toggling  -->*/
/*                                 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/*                                     <ul class="nav navbar-nav navbar-right">*/
/*                                         <li><a href="{{ path('_welcome') }}">Home</a></li>*/
/*                                         <li><a href="#">{% trans %}Po pracy{% endtrans %}</a></li>*/
/*                                         <li><a href="#">Kontakt</a></li>*/
/*                                         <li><a href="#">Regulamin</a></li>*/
/*                                         <li><a href="#">O nas</a></li>*/
/*                                     </ul>*/
/*                                 </div>*/
/*                                 <!-- /.navbar-collapse  -->*/
/*                             </div>*/
/*                             <!-- /.container-fluid -->*/
/*                         </nav>*/
/*                     </div>*/
/*                 </div>*/
/*             </section>*/
/*         {% endblock %}*/
/*         {% block body %}{% endblock %}*/
/*         <!--   Contact section      -->*/
/*         <section id="contact">*/
/*             <div class="container catalog">*/
/*                 <div class="row">*/
/*                     <div class="col-md-12">*/
/*                         <h2 class="text-center">*/
/*                         Formularz kontaktowy*/
/*                         <div class="space-100"></div>*/
/*                         <div class="row">*/
/*                             <div class="col-sm-8">*/
/*                                 <div class="row">*/
/*                                     <form id="form-javascript" action="{{ path('_test') }}" method="post">*/
/*                                         <div class="col-sm-6">*/
/*                                             <div class="form-group">*/
/*                                                 <input placeholder="Imię i nazwisko" required name="name" type="text" class="form-control" id="usr">*/
/*                                                 <p class="alert-form"></p>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="col-sm-6">*/
/*                                             <div class="form-group">*/
/*                                                 <input placeholder="Nazwa firmy" required name="name_company" type="text" class="form-control" id="pwd">*/
/*                                                 <p class="alert-form"></p>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="col-sm-6">*/
/*                                             <div class="form-group">*/
/*                                                 <input placeholder="Numer telefonu" required name="tel_number" type="tel" class="form-control" id="usr">*/
/*                                                 <p class="alert-form"></p>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="col-sm-6">*/
/*                                             <div class="form-group">*/
/*                                                 <input placeholder="Adres email" required name="email_adres" type="email" class="form-control" id="pwd">*/
/*                                                 <p class="alert-form"></p>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="col-sm-12">*/
/*                                             <div class="form-group">*/
/*                                                 <textarea placeholder="Treść wiadomości" name="content_messege" class="form-control" rows="9" id="comment"></textarea>*/
/*                                                 <p class="alert-form"></p>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="col-sm-3">*/
/*                                             <div class="space-20"></div>*/
/*                                             <button type="submit" id="form-button-main" class="btn pull-left btn-default-catalogy">Wyślij</button>*/
/*                                         </div>*/
/*                                     </form>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="col-sm-4">*/
/*                                 <div class="contact-right">*/
/*                                     <div class="contact-right-logo">*/
/*                                         <img src="{{ asset('bundles/framework/img/Vector-Smart-Object-email.png')}}" alt="adres" class="img-responsive">*/
/*                                     </div>*/
/*                                     <div class="contact-right-title">*/
/*                                         <h3>dane <br> teleadresowe</h3>*/
/*                                         <p>kontakt z właścicielem serwisu</p>*/
/*                                     </div>*/
/*                                     <div class="contact-right-info">*/
/*                                         <p><a href="tel: +48 600 215 364">+48 600 215 364</a></p>*/
/*                                         <p><a href="mailto: contact@catalog.com">contact@catalog.com</a></p>*/
/*                                         <div class="space-10"></div>*/
/*                                         <p>Wolsztyńska 18</p>*/
/*                                         <p>65-415 Wrocław</p>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                         </h2>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </section>*/
/*         <footer>*/
/*             <div class="container catalog">*/
/*                 <div class="row">*/
/*                     <div class="col-sm-4">{% block footer_info %}All rights reserved by catalog.com{% endblock %}</div>*/
/*                     <div class="col-sm-8 text-right"><a href="#nav-section-up">Up</a></div>*/
/*                 </div>*/
/*             </div>*/
/*         </footer>*/
/*         <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"/></script>*/
/*         <script src="{{asset('bundles/framework/js/vendor/bootstrap.min.js')}}"/></script>*/
/*         <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>*/
/*         <script>*/
/*             var map;*/
/*             var myCenter = new google.maps.LatLng(53, -1.33);*/
/*             var marker = new google.maps.Marker({*/
/*                 position: myCenter*/
/*             });*/
/* */
/*             function initialize() {*/
/*                 var mapProp = {*/
/*                     center: myCenter,*/
/*                     zoom: 14,*/
/*                     draggable: false,*/
/*                     scrollwheel: false,*/
/*                     mapTypeId: google.maps.MapTypeId.ROADMAP*/
/*                 };*/
/* */
/*                 map = new google.maps.Map(document.getElementById("map-canvas"), mapProp);*/
/*                 marker.setMap(map);*/
/* */
/*                 google.maps.event.addListener(marker, 'click', function () {*/
/* */
/*                     infowindow.setContent(contentString);*/
/*                     infowindow.open(map, marker);*/
/* */
/*                 });*/
/*             };*/
/*             google.maps.event.addDomListener(window, 'load', initialize);*/
/* */
/*             google.maps.event.addDomListener(window, "resize", resizingMap());*/
/* */
/*             $('#myMapModal').on('show.bs.modal', function () {*/
/*                 //Must wait until the render of the modal appear, thats why we use the resizeMap and NOT resizingMap!! ;-)*/
/*                 resizeMap();*/
/*             })*/
/* */
/*             function resizeMap() {*/
/*                 if (typeof map == "undefined") return;*/
/*                 setTimeout(function () {*/
/*                     resizingMap();*/
/*                 }, 400);*/
/*             }*/
/* */
/*             function resizingMap() {*/
/*                 if (typeof map == "undefined") return;*/
/*                 var center = map.getCenter();*/
/*                 google.maps.event.trigger(map, "resize");*/
/*                 map.setCenter(center);*/
/*             }*/
/*         </script>*/
/*         <script src="{{ asset('bundles/framework/js/main.js') }}"/></scirpt>*/
/*         {% block javascripts_user %}*/
/* */
/*         {% endblock %}*/
/*     </body>*/
/* </html>*/
