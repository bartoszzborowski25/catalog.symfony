<?php

/* :start:index.html.twig */
class __TwigTemplate_ee326a2c9f1224dcec9a018b8b31adbe0f273a5580ee7deae98157af20171227 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":start:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <section id=\"top-slide\">
        <div class=\"container catalog\">
            <div class=\"row\">
                <div class=\"col-sm-12\">
                    <h1>
                        Rozwijająca się firma Magic Moon LTD pragnie 
                    dać Wam możliwość zobaczenia oraz zakupu
                     wyoskiej klasy produktów oferowanych przez
                    rosnącą listę europejskich producentów
                   </h1>
                    <h2>Dobre ceny mają tylko producenci</h2>
                    <a href=\"#our-producers\"><span>Przejdź dalej <br><i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i></span></a>
                </div>
            </div>
        </div>
    </section>

    <section id=\"trade\">
        <div class=\"container\">
            <div class=\"row\">
                <h2 class=\"text-center\">Działamy w branżach</h2>
                <div class=\"col-md-2\">
                    <a href=\"#our-producers\">
                        <div class=\"trade-box\">
                            <img class=\"img-responsive\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Vector-Smart-Object-copy-2.png"), "html", null, true);
        echo "\" alt=\"budownictwo\">
                        </div>
                    </a>
                    <h3>Budownictwo</h3>
                </div>
                <div class=\"col-md-2\">
                    <a href=\"#our-producers\">
                        <div class=\"trade-box\">
                            <img class=\"img-responsive\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Shape-7.png "), "html", null, true);
        echo "\" alt=\"artykuly-spozywcze\">
                        </div>
                    </a>
                    <h3 class=\"text-center\">Artykuły <br> spożywcze</h3>
                </div>
                <div class=\"col-md-2\">
                    <a href=\"#our-producers\">
                        <div class=\"trade-box\">
                            <img class=\"img-responsive\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Shape-10.png"), "html", null, true);
        echo "\" alt=\"transport\">
                        </div>
                    </a>
                    <h3 class=\"text-center\">Transport lądowy</h3>
                </div>
                <div class=\"col-md-2\">
                    <a href=\"#our-producers\">
                        <div class=\"trade-box\">
                            <img class=\"img-responsive\" src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Shape-12.png"), "html", null, true);
        echo "\" alt=\"wyposażanie-biur\">
                        </div>
                    </a>
                    <h3 class=\"text-center\">Wyposażanie biur</h3>
                </div>
                <div class=\"col-md-2\">
                    <a href=\"#our-producers\">
                        <div class=\"trade-box\">
                            <img class=\"img-responsive\" src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Shape-13.png"), "html", null, true);
        echo "\" alt=\"motoryzacja\">
                        </div>
                    </a>
                    <h3 class=\"text-center\">Motorozacyjna</h3>
                </div>
                <div class=\"col-md-2\">
                    <a href=\"#our-producers\">
                        <div class=\"trade-box\">
                            <img class=\"img-responsive\" src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Vector-Smart-Object.png"), "html", null, true);
        echo "\" alt=\"wykończenie-wnętrz\">
                        </div>
                    </a>
                    <h3 class=\"text-center\">Wykończenie wnętrz</h3>
                </div>
            </div>
        </div>
    </section>

    <!--    Our producers     -->
    <section id=\"our-producers\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <h2>Lista naszych producentów</h2>
                    <p>Zasady zamówienia patrze regulamin</p>
                    <div class=\"row\">
                        <div class=\"col-sm-6\">
                            <a href=\"";
        // line 86
        echo $this->env->getExtension('routing')->getPath("_producers");
        echo "\">
                                <div class=\"producers-box\">
                                    <div class=\"producers-box-title-top\">
                                        <h3>Budownictwo</h3>
                                    </div>
                                    <div class=\"producers-box-title\">
                                        <h3>Meraki</h3>
                                    </div>
                                    <div class=\"producers-box-logo\">
                                        <img src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/producers/_logotype.png"), "html", null, true);
        echo "\" alt=\"meraki\" class=\"img-reponsive\">
                                    </div>
                                    <div class=\"producers-box-body\">
                                        <p>Zakład Produkcji Materiałów Budowlanych Meraki w Lubartowie powstał w 1992 roku. Od początku działalności specjalizował się w produkcji stropów.</p>
                                    </div>
                                    <div class=\"producers-box-title-bottom\">
                                        <h4 class=\"text-center\">kliknij aby zobaczyć szczegóły</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class=\"col-sm-6\">
                            <a href=\"";
        // line 107
        echo $this->env->getExtension('routing')->getPath("_producers");
        echo "\">
                                <div class=\"producers-box\">
                                    <div class=\"producers-box-title-top\">
                                        <h3>Budownictwo</h3>
                                    </div>
                                    <div class=\"producers-box-title\">
                                        <h3>Meraki</h3>
                                    </div>
                                    <div class=\"producers-box-logo\">
                                        <img src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/producers/_logotype.png"), "html", null, true);
        echo "\" alt=\"meraki\" class=\"img-reponsive\">
                                    </div>
                                    <div class=\"producers-box-body\">
                                        <p>Zakład Produkcji Materiałów Budowlanych Meraki w Lubartowie powstał w 1992 roku. Od początku działalności specjalizował się w produkcji stropów.</p>
                                    </div>
                                    <div class=\"producers-box-title-bottom\">
                                        <h4 class=\"text-center\">kliknij aby zobaczyć szczegóły</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class=\"col-sm-6\">
                            <a href=\"";
        // line 128
        echo $this->env->getExtension('routing')->getPath("_producers");
        echo "\">
                                <div class=\"producers-box\">
                                    <div class=\"producers-box-title-top\">
                                        <h3>Budownictwo</h3>
                                    </div>
                                    <div class=\"producers-box-title\">
                                        <h3>Meraki</h3>
                                    </div>
                                    <div class=\"producers-box-logo\">
                                        <img src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/producers/_logotype.png"), "html", null, true);
        echo "\" alt=\"meraki\" class=\"img-reponsive\">
                                    </div>
                                    <div class=\"producers-box-body\">
                                        <p>Zakład Produkcji Materiałów Budowlanych Meraki w Lubartowie powstał w 1992 roku. Od początku działalności specjalizował się w produkcji stropów.</p>
                                    </div>
                                    <div class=\"producers-box-title-bottom\">
                                        <h4 class=\"text-center\">kliknij aby zobaczyć szczegóły</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class=\"col-sm-6\">
                            <a href=\"";
        // line 149
        echo $this->env->getExtension('routing')->getPath("_producers");
        echo "\">
                                <div class=\"producers-box\">
                                    <div class=\"producers-box-title-top\">
                                        <h3>Budownictwo</h3>
                                    </div>
                                    <div class=\"producers-box-title\">
                                        <h3>Meraki</h3>
                                    </div>
                                    <div class=\"producers-box-logo\">
                                        <img src=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/producers/_logotype.png"), "html", null, true);
        echo "\" alt=\"meraki\" class=\"img-reponsive\">
                                    </div>
                                    <div class=\"producers-box-body\">
                                        <p>Zakład Produkcji Materiałów Budowlanych Meraki w Lubartowie powstał w 1992 roku. Od początku działalności specjalizował się w produkcji stropów.</p>
                                    </div>
                                    <div class=\"producers-box-title-bottom\">
                                        <h4 class=\"text-center\">kliknij aby zobaczyć szczegóły</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
";
    }

    public function getTemplateName()
    {
        return ":start:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 158,  214 => 149,  199 => 137,  187 => 128,  172 => 116,  160 => 107,  145 => 95,  133 => 86,  112 => 68,  101 => 60,  90 => 52,  79 => 44,  68 => 36,  57 => 28,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <section id="top-slide">*/
/*         <div class="container catalog">*/
/*             <div class="row">*/
/*                 <div class="col-sm-12">*/
/*                     <h1>*/
/*                         Rozwijająca się firma Magic Moon LTD pragnie */
/*                     dać Wam możliwość zobaczenia oraz zakupu*/
/*                      wyoskiej klasy produktów oferowanych przez*/
/*                     rosnącą listę europejskich producentów*/
/*                    </h1>*/
/*                     <h2>Dobre ceny mają tylko producenci</h2>*/
/*                     <a href="#our-producers"><span>Przejdź dalej <br><i class="fa fa-angle-down" aria-hidden="true"></i></span></a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </section>*/
/* */
/*     <section id="trade">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <h2 class="text-center">Działamy w branżach</h2>*/
/*                 <div class="col-md-2">*/
/*                     <a href="#our-producers">*/
/*                         <div class="trade-box">*/
/*                             <img class="img-responsive" src="{{ asset('bundles/framework/img/Vector-Smart-Object-copy-2.png') }}" alt="budownictwo">*/
/*                         </div>*/
/*                     </a>*/
/*                     <h3>Budownictwo</h3>*/
/*                 </div>*/
/*                 <div class="col-md-2">*/
/*                     <a href="#our-producers">*/
/*                         <div class="trade-box">*/
/*                             <img class="img-responsive" src="{{ asset('bundles/framework/img/Shape-7.png ')}}" alt="artykuly-spozywcze">*/
/*                         </div>*/
/*                     </a>*/
/*                     <h3 class="text-center">Artykuły <br> spożywcze</h3>*/
/*                 </div>*/
/*                 <div class="col-md-2">*/
/*                     <a href="#our-producers">*/
/*                         <div class="trade-box">*/
/*                             <img class="img-responsive" src="{{ asset('bundles/framework/img/Shape-10.png')}}" alt="transport">*/
/*                         </div>*/
/*                     </a>*/
/*                     <h3 class="text-center">Transport lądowy</h3>*/
/*                 </div>*/
/*                 <div class="col-md-2">*/
/*                     <a href="#our-producers">*/
/*                         <div class="trade-box">*/
/*                             <img class="img-responsive" src="{{ asset('bundles/framework/img/Shape-12.png')}}" alt="wyposażanie-biur">*/
/*                         </div>*/
/*                     </a>*/
/*                     <h3 class="text-center">Wyposażanie biur</h3>*/
/*                 </div>*/
/*                 <div class="col-md-2">*/
/*                     <a href="#our-producers">*/
/*                         <div class="trade-box">*/
/*                             <img class="img-responsive" src="{{ asset('bundles/framework/img/Shape-13.png')}}" alt="motoryzacja">*/
/*                         </div>*/
/*                     </a>*/
/*                     <h3 class="text-center">Motorozacyjna</h3>*/
/*                 </div>*/
/*                 <div class="col-md-2">*/
/*                     <a href="#our-producers">*/
/*                         <div class="trade-box">*/
/*                             <img class="img-responsive" src="{{ asset('bundles/framework/img/Vector-Smart-Object.png')}}" alt="wykończenie-wnętrz">*/
/*                         </div>*/
/*                     </a>*/
/*                     <h3 class="text-center">Wykończenie wnętrz</h3>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </section>*/
/* */
/*     <!--    Our producers     -->*/
/*     <section id="our-producers">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <h2>Lista naszych producentów</h2>*/
/*                     <p>Zasady zamówienia patrze regulamin</p>*/
/*                     <div class="row">*/
/*                         <div class="col-sm-6">*/
/*                             <a href="{{ path('_producers') }}">*/
/*                                 <div class="producers-box">*/
/*                                     <div class="producers-box-title-top">*/
/*                                         <h3>Budownictwo</h3>*/
/*                                     </div>*/
/*                                     <div class="producers-box-title">*/
/*                                         <h3>Meraki</h3>*/
/*                                     </div>*/
/*                                     <div class="producers-box-logo">*/
/*                                         <img src="{{ asset('bundles/framework/img/producers/_logotype.png')}}" alt="meraki" class="img-reponsive">*/
/*                                     </div>*/
/*                                     <div class="producers-box-body">*/
/*                                         <p>Zakład Produkcji Materiałów Budowlanych Meraki w Lubartowie powstał w 1992 roku. Od początku działalności specjalizował się w produkcji stropów.</p>*/
/*                                     </div>*/
/*                                     <div class="producers-box-title-bottom">*/
/*                                         <h4 class="text-center">kliknij aby zobaczyć szczegóły</h4>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </a>*/
/*                         </div>*/
/*                         <div class="col-sm-6">*/
/*                             <a href="{{ path('_producers') }}">*/
/*                                 <div class="producers-box">*/
/*                                     <div class="producers-box-title-top">*/
/*                                         <h3>Budownictwo</h3>*/
/*                                     </div>*/
/*                                     <div class="producers-box-title">*/
/*                                         <h3>Meraki</h3>*/
/*                                     </div>*/
/*                                     <div class="producers-box-logo">*/
/*                                         <img src="{{ asset('bundles/framework/img/producers/_logotype.png')}}" alt="meraki" class="img-reponsive">*/
/*                                     </div>*/
/*                                     <div class="producers-box-body">*/
/*                                         <p>Zakład Produkcji Materiałów Budowlanych Meraki w Lubartowie powstał w 1992 roku. Od początku działalności specjalizował się w produkcji stropów.</p>*/
/*                                     </div>*/
/*                                     <div class="producers-box-title-bottom">*/
/*                                         <h4 class="text-center">kliknij aby zobaczyć szczegóły</h4>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </a>*/
/*                         </div>*/
/*                         <div class="col-sm-6">*/
/*                             <a href="{{ path('_producers') }}">*/
/*                                 <div class="producers-box">*/
/*                                     <div class="producers-box-title-top">*/
/*                                         <h3>Budownictwo</h3>*/
/*                                     </div>*/
/*                                     <div class="producers-box-title">*/
/*                                         <h3>Meraki</h3>*/
/*                                     </div>*/
/*                                     <div class="producers-box-logo">*/
/*                                         <img src="{{ asset('bundles/framework/img/producers/_logotype.png')}}" alt="meraki" class="img-reponsive">*/
/*                                     </div>*/
/*                                     <div class="producers-box-body">*/
/*                                         <p>Zakład Produkcji Materiałów Budowlanych Meraki w Lubartowie powstał w 1992 roku. Od początku działalności specjalizował się w produkcji stropów.</p>*/
/*                                     </div>*/
/*                                     <div class="producers-box-title-bottom">*/
/*                                         <h4 class="text-center">kliknij aby zobaczyć szczegóły</h4>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </a>*/
/*                         </div>*/
/*                         <div class="col-sm-6">*/
/*                             <a href="{{ path('_producers') }}">*/
/*                                 <div class="producers-box">*/
/*                                     <div class="producers-box-title-top">*/
/*                                         <h3>Budownictwo</h3>*/
/*                                     </div>*/
/*                                     <div class="producers-box-title">*/
/*                                         <h3>Meraki</h3>*/
/*                                     </div>*/
/*                                     <div class="producers-box-logo">*/
/*                                         <img src="{{ asset('bundles/framework/img/producers/_logotype.png')}}" alt="meraki" class="img-reponsive">*/
/*                                     </div>*/
/*                                     <div class="producers-box-body">*/
/*                                         <p>Zakład Produkcji Materiałów Budowlanych Meraki w Lubartowie powstał w 1992 roku. Od początku działalności specjalizował się w produkcji stropów.</p>*/
/*                                     </div>*/
/*                                     <div class="producers-box-title-bottom">*/
/*                                         <h4 class="text-center">kliknij aby zobaczyć szczegóły</h4>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </a>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </section>*/
/* {% endblock %}*/
/* */
/* */
