<?php

/* :start:producers.html.twig */
class __TwigTemplate_4ea44c5774bfc8597e59f71fcc73e026a6c205f90ca09f21ef6a22548078d950 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":start:producers.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts_user' => array($this, 'block_javascripts_user'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo " <!--   Producers info section      -->
    <section id=\"producers-info\">
        <div class=\"container producers-container\">
            <div class=\"row\">
                <div class=\"col-sm-12\">
                    <small>Branża: Budownictwo</small>
                    <h2>Koelner Łańcucka Fabryka Śrub</h2>
                    <div class=\"producers-detail-image\">
                        <img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Layer-255.png"), "html", null, true);
        echo "\" alt=\"firma\" class=\"img-responsive\">
                    </div>
                    <div class=\"row\">
                        <div class=\"col-sm-6\">
                            <button type=\"button\" class=\"btn\"><img src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Forma-1.png"), "html", null, true);
        echo "\" alt=\"forma\" class=\"img-responsive\">&nbsp; Pobierz ofertę handlową</button>
                        </div>
                        <div class=\"col-sm-6\">
                            <button data-toggle=\"modal\" data-target=\"#myMapModal\" type=\"button\" class=\"btn\">Pokaż lokalizację firmy</button>
                        </div>
                    </div>
                    <a href=\"#\"><span>Przejdź dalej <br><i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i></span></a>
                    <div class=\"producers-detail-content\">
                        <h3>Od 1 stycznia 2009 roku działalność Fabryki Śrub w Łańcucie ŚRUBEX S.A. została przeniesiona do 
                        powołanej w tym celu firmy KOELNER Łańcucka 
                        Fabryka Śrub. </h3>
                        <p>
                            W ten sposób nowy podmiot stał się następcą ponad pięćdziesięcioletniej tradycji firmy, działającej na rynku od 1957 r.
                            <br>
                            <br> Dziś jesteśmy największym producentem elementów złącznych w Europie Środkowo Wschodniej i liderem rynku polskiego. Znak towarowy ŁF jest rozpoznawalny nie tylko w Polsce, ale także na świecie i kojarzony z wysoką jakością wyrobów śrubowych.
                            <br>
                            <br> Odbiorcami naszych produktów są przedsiębiorstwa ze wszystkich sektorów przemysłu, a w szczególności z takich branż jak motoryzacja, budownictwo, przemysł maszynowy i elektromaszynowy, przemysł wydobywczy, meblarstwo, energetyka, przemysł stoczniowy oraz drogowy.
                            <br>
                            <br> Oferta handlowa obejmuje: śruby w zakresie od M4 do M24, nakrętki w zakresach od M4 do M20, wyroby specjalne oraz inne elementy złączne o bardziej skomplikowanym rysunku. Oferta wyrobów specjalnych pozwala na ich wykonanie według indywidualnych potrzeb klientów. Każdego roku wprowadzamy kilkadziesiąt nowych produktów będących odpowiedzią na sugestie naszych odbiorców. Zapewniamy również obróbkę cieplną i różne rodzaje pokryć ochronnych.
                            <br>
                            <br> Zapraszamy do zapoznania się z naszą ofertą w wirtualnym kata
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div id=\"producers-oferts\">
            <div class=\"container container-producers-oferts\">
                <h2>Oferowane produkty</h2>
                <div class=\"row\">
                    <div class=\"col-md-4\">
                        <div class=\"producers-oferts-box\">
                            <div class=\"producers-oferts-oferts-top\">
                                <img src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Layer-241.png"), "html", null, true);
        echo "\" alt=\"produkty\" class=\"img-responsive\">
                            </div>
                            <div class=\"producers-oferts-oferts-bottom\">
                                <small>śruby</small>
                                <h3>Din 6900</h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4\">
                        <div class=\"producers-oferts-box\">
                            <div class=\"producers-oferts-oferts-top\">
                                <img src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Layer-241.png"), "html", null, true);
        echo "\" alt=\"produkty\" class=\"img-responsive\">
                            </div>
                            <div class=\"producers-oferts-oferts-bottom\">
                                <small>śruby</small>
                                <h3>Din 6900</h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4\">
                        <div class=\"producers-oferts-box\">
                            <div class=\"producers-oferts-oferts-top\">
                                <img src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Layer-241.png"), "html", null, true);
        echo "\" alt=\"produkty\" class=\"img-responsive\">
                            </div>
                            <div class=\"producers-oferts-oferts-bottom\">
                                <small>śruby</small>
                                <h3>Din 6900</h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4\">
                        <div class=\"producers-oferts-box\">
                            <div class=\"producers-oferts-oferts-top\">
                                <img src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Layer-241.png"), "html", null, true);
        echo "\" alt=\"produkty\" class=\"img-responsive\">
                            </div>
                            <div class=\"producers-oferts-oferts-bottom\">
                                <small>śruby</small>
                                <h3>Din 6900</h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4\">
                        <div class=\"producers-oferts-box\">
                            <div class=\"producers-oferts-oferts-top\">
                                <img src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Layer-241.png"), "html", null, true);
        echo "\" alt=\"produkty\" class=\"img-responsive\">
                            </div>
                            <div class=\"producers-oferts-oferts-bottom\">
                                <small>śruby</small>
                                <h3>Din 6900</h3>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4\">
                        <div class=\"producers-oferts-box\">
                            <div class=\"producers-oferts-oferts-top\">
                                <img src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Layer-241.png"), "html", null, true);
        echo "\" alt=\"produkty\" class=\"img-responsive\">
                            </div>
                            <div class=\"producers-oferts-oferts-bottom\">
                                <small>śruby</small>
                                <h3>Din 6900</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <button type=\"button\" class=\"btn\"><img src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/Forma-1.png"), "html", null, true);
        echo " \" alt=\"forma\"> &nbsp;Pobierz pełną ofertę</button>
            </div>
        </div>
    </section>

    <section id=\"tranparent-bac\"></section>

    <div class=\"modal fade bs-example-modal-lg\" id=\"myMapModal\">
        <div class=\"modal-dialog modal-lg\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                </div>
                <div class=\"modal-body\">
                    <div id=\"map-canvas\" class=\"\"></div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

";
    }

    // line 136
    public function block_javascripts_user($context, array $blocks = array())
    {
        // line 137
        echo "
";
    }

    public function getTemplateName()
    {
        return ":start:producers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 137,  194 => 136,  167 => 113,  155 => 104,  141 => 93,  127 => 82,  113 => 71,  99 => 60,  85 => 49,  49 => 16,  42 => 12,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*  <!--   Producers info section      -->*/
/*     <section id="producers-info">*/
/*         <div class="container producers-container">*/
/*             <div class="row">*/
/*                 <div class="col-sm-12">*/
/*                     <small>Branża: Budownictwo</small>*/
/*                     <h2>Koelner Łańcucka Fabryka Śrub</h2>*/
/*                     <div class="producers-detail-image">*/
/*                         <img src="{{ asset('bundles/framework/img/Layer-255.png')}}" alt="firma" class="img-responsive">*/
/*                     </div>*/
/*                     <div class="row">*/
/*                         <div class="col-sm-6">*/
/*                             <button type="button" class="btn"><img src="{{ asset('bundles/framework/img/Forma-1.png')}}" alt="forma" class="img-responsive">&nbsp; Pobierz ofertę handlową</button>*/
/*                         </div>*/
/*                         <div class="col-sm-6">*/
/*                             <button data-toggle="modal" data-target="#myMapModal" type="button" class="btn">Pokaż lokalizację firmy</button>*/
/*                         </div>*/
/*                     </div>*/
/*                     <a href="#"><span>Przejdź dalej <br><i class="fa fa-angle-down" aria-hidden="true"></i></span></a>*/
/*                     <div class="producers-detail-content">*/
/*                         <h3>Od 1 stycznia 2009 roku działalność Fabryki Śrub w Łańcucie ŚRUBEX S.A. została przeniesiona do */
/*                         powołanej w tym celu firmy KOELNER Łańcucka */
/*                         Fabryka Śrub. </h3>*/
/*                         <p>*/
/*                             W ten sposób nowy podmiot stał się następcą ponad pięćdziesięcioletniej tradycji firmy, działającej na rynku od 1957 r.*/
/*                             <br>*/
/*                             <br> Dziś jesteśmy największym producentem elementów złącznych w Europie Środkowo Wschodniej i liderem rynku polskiego. Znak towarowy ŁF jest rozpoznawalny nie tylko w Polsce, ale także na świecie i kojarzony z wysoką jakością wyrobów śrubowych.*/
/*                             <br>*/
/*                             <br> Odbiorcami naszych produktów są przedsiębiorstwa ze wszystkich sektorów przemysłu, a w szczególności z takich branż jak motoryzacja, budownictwo, przemysł maszynowy i elektromaszynowy, przemysł wydobywczy, meblarstwo, energetyka, przemysł stoczniowy oraz drogowy.*/
/*                             <br>*/
/*                             <br> Oferta handlowa obejmuje: śruby w zakresie od M4 do M24, nakrętki w zakresach od M4 do M20, wyroby specjalne oraz inne elementy złączne o bardziej skomplikowanym rysunku. Oferta wyrobów specjalnych pozwala na ich wykonanie według indywidualnych potrzeb klientów. Każdego roku wprowadzamy kilkadziesiąt nowych produktów będących odpowiedzią na sugestie naszych odbiorców. Zapewniamy również obróbkę cieplną i różne rodzaje pokryć ochronnych.*/
/*                             <br>*/
/*                             <br> Zapraszamy do zapoznania się z naszą ofertą w wirtualnym kata*/
/*                         </p>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div id="producers-oferts">*/
/*             <div class="container container-producers-oferts">*/
/*                 <h2>Oferowane produkty</h2>*/
/*                 <div class="row">*/
/*                     <div class="col-md-4">*/
/*                         <div class="producers-oferts-box">*/
/*                             <div class="producers-oferts-oferts-top">*/
/*                                 <img src="{{ asset('bundles/framework/img/Layer-241.png')}}" alt="produkty" class="img-responsive">*/
/*                             </div>*/
/*                             <div class="producers-oferts-oferts-bottom">*/
/*                                 <small>śruby</small>*/
/*                                 <h3>Din 6900</h3>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4">*/
/*                         <div class="producers-oferts-box">*/
/*                             <div class="producers-oferts-oferts-top">*/
/*                                 <img src="{{ asset('bundles/framework/img/Layer-241.png')}}" alt="produkty" class="img-responsive">*/
/*                             </div>*/
/*                             <div class="producers-oferts-oferts-bottom">*/
/*                                 <small>śruby</small>*/
/*                                 <h3>Din 6900</h3>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4">*/
/*                         <div class="producers-oferts-box">*/
/*                             <div class="producers-oferts-oferts-top">*/
/*                                 <img src="{{ asset('bundles/framework/img/Layer-241.png')}}" alt="produkty" class="img-responsive">*/
/*                             </div>*/
/*                             <div class="producers-oferts-oferts-bottom">*/
/*                                 <small>śruby</small>*/
/*                                 <h3>Din 6900</h3>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4">*/
/*                         <div class="producers-oferts-box">*/
/*                             <div class="producers-oferts-oferts-top">*/
/*                                 <img src="{{ asset('bundles/framework/img/Layer-241.png')}}" alt="produkty" class="img-responsive">*/
/*                             </div>*/
/*                             <div class="producers-oferts-oferts-bottom">*/
/*                                 <small>śruby</small>*/
/*                                 <h3>Din 6900</h3>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4">*/
/*                         <div class="producers-oferts-box">*/
/*                             <div class="producers-oferts-oferts-top">*/
/*                                 <img src="{{ asset('bundles/framework/img/Layer-241.png')}}" alt="produkty" class="img-responsive">*/
/*                             </div>*/
/*                             <div class="producers-oferts-oferts-bottom">*/
/*                                 <small>śruby</small>*/
/*                                 <h3>Din 6900</h3>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4">*/
/*                         <div class="producers-oferts-box">*/
/*                             <div class="producers-oferts-oferts-top">*/
/*                                 <img src="{{ asset('bundles/framework/img/Layer-241.png')}}" alt="produkty" class="img-responsive">*/
/*                             </div>*/
/*                             <div class="producers-oferts-oferts-bottom">*/
/*                                 <small>śruby</small>*/
/*                                 <h3>Din 6900</h3>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <button type="button" class="btn"><img src="{{ asset('bundles/framework/img/Forma-1.png')}} " alt="forma"> &nbsp;Pobierz pełną ofertę</button>*/
/*             </div>*/
/*         </div>*/
/*     </section>*/
/* */
/*     <section id="tranparent-bac"></section>*/
/* */
/*     <div class="modal fade bs-example-modal-lg" id="myMapModal">*/
/*         <div class="modal-dialog modal-lg">*/
/*             <div class="modal-content">*/
/*                 <div class="modal-header">*/
/*                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>*/
/*                 </div>*/
/*                 <div class="modal-body">*/
/*                     <div id="map-canvas" class=""></div>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- /.modal-content -->*/
/*         </div>*/
/*         <!-- /.modal-dialog -->*/
/*     </div>*/
/* */
/* {% endblock %}*/
/* {% block javascripts_user %}*/
/* */
/* {% endblock %}*/
/* */
