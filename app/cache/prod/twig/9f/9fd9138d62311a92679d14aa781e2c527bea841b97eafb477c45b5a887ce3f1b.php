<?php

/* :emails:main.html.twig */
class __TwigTemplate_ba121bd69a6a1d0767c23cd3e8befd539f022b73c9984d2a7eb291a7114de350 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<h3>You did it! You registered!</h3>

Hi! You're successfully registered.

";
        // line 8
        echo "
Thanks!

";
    }

    public function getTemplateName()
    {
        return ":emails:main.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 8,  19 => 2,);
    }
}
/* {# app/Resources/views/Emails/registration.html.twig #}*/
/* <h3>You did it! You registered!</h3>*/
/* */
/* Hi! You're successfully registered.*/
/* */
/* {# example, assuming you have a route named "login" #}*/
/* {#To login, go to: <a href="{{ url('login') }}">...</a>.#}*/
/* */
/* Thanks!*/
/* */
/* {# Makes an absolute URL to the /images/logo.png file #}*/
/* {# <img src="{{ absolute_url(asset('images/logo.png')) }}"> #}*/
