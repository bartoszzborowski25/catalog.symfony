function menufixed (boolen_menu) {
    if (boolen_menu)
        $("#nav").addClass("menu-fixed");
    else 
        $("#nav").removeClass("menu-fixed");
        
}

$(window).scroll(function(){
    var height_body = $('body').scrollTop();
    if (height_body >= 40)
        menufixed(true);
    else 
        menufixed(false);
})
$(document).ready(function(){

/* ==========================================================================
   Płynne Przewijanie
   ========================================================================== */

	$('a[href^="#"]').on('click', function(event) {

		var target = $( $(this).attr('href') );

		if( target.length ) {
			event.preventDefault();
			$('html, body').animate({
				scrollTop: target.offset().top
			}, 1500);
		}
	});

    // $("#form-javascript").each(function(index, el) {
    //     console.log($(this).find('input').val());
    // });
    $("button#form-button-main").attr("disabled", true);
    $("input[name='name']").focusout(function(event) {
    });
    $("input[name='name_company']").focusout(function(event) {
        console.log($(this).val());
    });
    $("input[name='tel_number']").focusout(function(event) {
        console.log($(this).val());
    });
    $("input[name='email_adres']").focusout(function(event) {
        console.log($(this).val());
    });
    $("textarea#comment").keyup(function(event) {
        if ($(this).val().length<=10) {
            $(this).next('.alert-form').text("Wiadomość jest za krótka! (Min: 10 znaków)");
            $("button#form-button-main").attr("disabled", true);
        }
        else {
            $(this).next('.alert-form').text("");
            $("button#form-button-main").attr("disabled", false);
        }
    });

});

function GetCookie (name) {
var arg = name + "=";
var alen = arg.length;
var clen = document.cookie.length;
var i = 0;
while (i < clen) {
var j = i + alen;
if (document.cookie.substring(i, j) == arg)
return getCookieVal (j);
i = document.cookie.indexOf(" ", i) + 1;
if (i == 0) break;
}
return null;
}
function SetCookie (name, value) {
var argv = SetCookie.arguments;
var argc = SetCookie.arguments.length;
var expires = (argc > 2) ? argv[2] : null;
var path = (argc > 3) ? argv[3] : null;
var domain = (argc > 4) ? argv[4] : null;
var secure = (argc > 5) ? argv[5] : false;
document.cookie = name + "=" + escape (value) +
((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
((path == null) ? "" : ("; path=" + path)) +
((domain == null) ? "" : ("; domain=" + domain)) +
((secure == true) ? "; secure" : "");
}
function DeleteCookie (name) {
var exp = new Date();
exp.setTime (exp.getTime() - 1);
var cval = GetCookie (name);
document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
}
var expDays = 30;
var exp = new Date();
exp.setTime(exp.getTime() + (expDays*24*60*60*1000));
function amt(){
var count = GetCookie('count')
if(count == null) {
SetCookie('count','1')
return 1
}
else {
var newcount = parseInt(count) + 1;
DeleteCookie('count')
SetCookie('count',newcount,exp)
return count
}
}
function getCookieVal(offset) {
var endstr = document.cookie.indexOf (";", offset);
if (endstr == -1)
endstr = document.cookie.length;
return unescape(document.cookie.substring(offset, endstr));
}

if(amt()==1)
{
alert('Jesteś tutaj pierwszy raz!');//alert
}
