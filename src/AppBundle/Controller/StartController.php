<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class StartController extends Controller
{
    /**
     * @Route(
     *     "/{_locale}",
     *     defaults={"_locale": "pl"},
     *     name="_welcome",
     *     requirements={
     *         "_locale": "pl|en"
     *     }
     * )
     */
    public function startAction($_locale)
    {
        return $this->render('start/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }
    /**
     * @Route(
     *     "/{_locale}/producers",
     *     defaults={"_locale": "pl"},
     *     name="_producers",
     *     requirements={
     *         "_locale": "pl|en"
     *     }
     * )
     */ 
    public function producersAction()
    {
        return $this->render('start/producers.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    } 
    /**
     * @Route(
     *     "/{_locale}/test",
     *     defaults={"_locale": "pl"},
     *     name="_test",
     *     requirements={
     *         "_locale": "pl|en"
     *     }
     * )
     */     
    public function newAction(Request $request)
    {
        $dane = [
            'name' => $request->request->get('name'),
            'name_company' => $request->request->get('name_company'),
            'tel_number' => $request->request->get('tel_number'),
            'email_adres' => $request->request->get('email_adres'),
            'content_messege' => $request->request->get('content_messege'),
        ];
        return $this->sendEmailAction($dane);  

    }  
    public function sendEmailAction($dane) 
    {

        $message = \Swift_Message::newInstance()
            ->setSubject('Hello Email')
            ->setFrom('send@example.com')
            ->setTo($dane['email_adres'])
            ->setBody(
                $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                    'emails/main.html.twig',
                    $dane
                ),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);

        return $this->redirectToRoute('_welcome');  

    }
}

?>